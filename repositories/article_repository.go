package repositories

import (
	"../models"
	"database/sql"
	"fmt"
	"time"
)

type articleRepoPostgres struct {
	db *sql.DB
}

func NewArticlePostgres(db *sql.DB) *articleRepoPostgres {
	return &articleRepoPostgres{db}
}

func (r *articleRepoPostgres) Save(article *models.Article) error {
	now := time.Now()

	query := "INSERT INTO article(creator_name, creator_img_url, title, text, image_url, category, created_at, updated_at) values($1, $2, $3, $4, $5, $6, $7, $8)"

	statement, err := r.db.Prepare(query)
	if err != nil {
		return err
	}
	//defer statement.Close()
	_, err = statement.Exec(article.CreatorName, article.CreatorImageUrl, article.Title, article.Text, article.ImageUrl, article.Category, now, now)
	if err != nil {
		return err
	}
	fmt.Println("\n\ninserting data")
	return nil
}

func (r *articleRepoPostgres) FindAll() (models.Articles, error) {
	query := `SELECT creator_name, creator_img_url, title, text, image_url, category FROM "article"`

	var articles models.Articles

	rows, err := r.db.Query(query)

	if err != nil {
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		var article models.Article

		err = rows.Scan(&article.CreatorName, &article.CreatorImageUrl, &article.Title, &article.Text, &article.ImageUrl, &article.Category)

		if err != nil {
			return nil, err
		}

		articles = append(articles, article)
	}

	return articles, nil
}

func (r *articleRepoPostgres) Update(id string, article *models.Article) error {

	query := `UPDATE "article" SET "creator_name"=$1, "creator_img_url"=$2, "title"=$3, "text"=$4, "image_url"=$5 WHERE "id"=$6`

	statement, err := r.db.Prepare(query)

	if err != nil {
		return err
	}

	defer statement.Close()

	_, err = statement.Exec(article.CreatorName, article.CreatorImageUrl, article.Title, article.Text, article.ImageUrl, id)

	if err != nil {
		return err
	}

	return nil
}

func (r *articleRepoPostgres) Delete(id string) error {

	query := `DELETE FROM "article" WHERE "id" = $1`

	statement, err := r.db.Prepare(query)

	if err != nil {
		return err
	}

	defer statement.Close()

	_, err = statement.Exec(id)

	if err != nil {
		return err
	}

	return nil
}

func (r *articleRepoPostgres) FindByID(id string) (*models.Article, error) {
	query := `SELECT creator_name, creator_img_url, title, text, image_url, category FROM "article" WHERE "id" = $1`

	var article models.Article

	statement, err := r.db.Prepare(query)

	if err != nil {
		return nil, err
	}

	defer statement.Close()

	err = statement.QueryRow(id).Scan(&article.CreatorName, &article.CreatorImageUrl, &article.Title, &article.Text, &article.ImageUrl, &article.Category, &article.CreatedAt, &article.UpdatedAt)

	if err != nil {
		return nil, err
	}

	return &article, nil

}
