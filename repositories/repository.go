package repositories

import (
	"../models"
)

type ArticleRepository interface {
	Save(*models.Article) error
	Update(string, *models.Article) error
	Delete(string) error
	FindByID(string) (*models.Article, error)
	FindAll() (models.Articles, error)
}
