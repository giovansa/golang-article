package main

import (
	"./models"
	"./repositories"
	"./routes"
	"./utils"
	_ "github.com/lib/pq"
	"net/http"
)

func main() {
	dbPostgres, err := models.GetPsql()
	if err != nil {
		panic(err)
	}

	utils.LoadTemplate("templates/*.html")
	router := routes.NewRouter(dbPostgres)
	http.Handle("/", router)
	http.ListenAndServe(":8888", nil)
}

func getArticles(repo repositories.ArticleRepository) (models.Articles, error) {
	articles, err := repo.FindAll()
	if err != nil {
		return nil, err
	}
	return articles, nil
}
