package routes

import (
	"../models"
	"../repositories"
	"../utils"
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"os"
	"strings"
)

var articleRepoPostgres repositories.ArticleRepository

func NewRouter(db *sql.DB) *mux.Router {
	//using db postgres
	var dbPostgres *sql.DB = db

	articleRepoPostgres = repositories.NewArticlePostgres(dbPostgres)

	router := mux.NewRouter()
	router.HandleFunc("/", handlerIndex).Methods("GET")
	apiRouter := router.PathPrefix("/api/").Subrouter()
	//API Handler
	apiRouter.HandleFunc("/articles", getArticles).Methods("GET")
	apiRouter.HandleFunc("/articles/{id}", getArticleById).Methods("GET")
	apiRouter.HandleFunc("/articles", createArticle).Methods("POST")
	apiRouter.HandleFunc("/articles/{id}", deleteArticle).Methods("DELETE")
	apiRouter.HandleFunc("/upload", postFile).Methods("POST")
	fs := http.FileServer(http.Dir("./files/"))
	router.PathPrefix("/files/").Handler(http.StripPrefix("/files/", fs))
	return router
}

func handlerIndex(w http.ResponseWriter, r *http.Request) {
	utils.ExecuteTemplate(w, "login.html", nil)
}

func getArticles(w http.ResponseWriter, r *http.Request) {
	articles, err := articleRepoPostgres.FindAll()
	if err != nil {
		fmt.Println(err)
	}
	encoder := json.NewEncoder(w)
	encoder.SetEscapeHTML(false)
	encoder.Encode(articles)
}

func createArticle(w http.ResponseWriter, r *http.Request) {
	article := models.NewArticle()

	reader, err := r.MultipartReader()
	if err != nil {
		fmt.Println("Multipartreader ", err)
		return
	}

	articleImgUrl := ""
	for {
		part, err := reader.NextPart()

		if err == io.EOF {
			break
		}
		fmt.Println(part.FormName() + "_")
		if part.FileName() == "" {
			continue
		}

		switch part.FormName() {
		case "article_images":

			//validate file extension for image
			if strings.HasSuffix(part.FileName(), ".png") || strings.HasSuffix(part.FileName(), ".jpg") || strings.HasSuffix(part.FileName(), ".jpeg") {

				dst, err := os.Create("./files/" + part.FileName())
				defer dst.Close()
				if err != nil {
					fmt.Println(err)
					return
				}
				if _, err := io.Copy(dst, part); err != nil {
					fmt.Println(err)
					return
				}
				articleImgUrl = articleImgUrl + "/files/" + part.FileName() + "#"
			}
		case "creator_image":

			//validate file extension for image
			if strings.HasSuffix(part.FileName(), ".png") || strings.HasSuffix(part.FileName(), ".jpg") || strings.HasSuffix(part.FileName(), ".jpeg") {

				dst, err := os.Create("./files/" + part.FileName())
				defer dst.Close()
				if err != nil {
					fmt.Println(err)
					return
				}
				if _, err := io.Copy(dst, part); err != nil {
					fmt.Println(err)
					return
				}
				article.CreatorImageUrl = "/files/" + part.FileName()
			}

		case "category":
			fmt.Println("test category")
			p := make([]byte, 4)
			n, err := part.Read(p)
			if err != nil {
				return
			}
			fmt.Println("\n\nHere the value")
			fmt.Println(string(p[:n]))
		case "title":
			fmt.Println("test title")
		case "text":
			fmt.Println("test text")
		case "creator_name":
			fmt.Println("test creator")
		}
	}
	article.ImageUrl = articleImgUrl
	r.ParseMultipartForm(0)
	article.CreatorName = r.FormValue("creator_name")
	article.Title = r.FormValue("title")
	article.Text = r.FormValue("text")
	article.Category = r.FormValue("category")

	err = articleRepoPostgres.Save(article)
	if err != nil {
		fmt.Println("repo save ", err)
	}
	jsonResponse(w, http.StatusCreated, "File Uploaded Successfuly")
}

func getArticleById(w http.ResponseWriter, r *http.Request) {

}

func deleteArticle(w http.ResponseWriter, r *http.Request) {

}

func postFile(w http.ResponseWriter, r *http.Request) {

	reader, err := r.MultipartReader()
	if err != nil {
		fmt.Println(err)
		return
	}
	for {
		part, err := reader.NextPart()
		if err == io.EOF {
			break
		}
		if part.FileName() == "" {
			continue
		}
		//validate file extension
		if strings.HasSuffix(part.FileName(), ".png") || strings.HasSuffix(part.FileName(), ".jpg") || strings.HasSuffix(part.FileName(), ".jpeg") {
			fmt.Println("pict")
			dst, err := os.Create("./files/" + part.FileName())
			defer dst.Close()
			if err != nil {
				fmt.Println(err)
				return
			}
			if _, err := io.Copy(dst, part); err != nil {
				fmt.Println(err)
				return
			}
		}

	}
	jsonResponse(w, http.StatusCreated, "File Uploaded Successfuly")
}

func saveFile(w http.ResponseWriter, file multipart.File, handle *multipart.FileHeader) string {
	data, err := ioutil.ReadAll(file)
	if err != nil {
		fmt.Println(err)
	}
	err = ioutil.WriteFile("./files/"+handle.Filename, data, 0666)
	if err != nil {
		fmt.Println(err)

	}
	jsonResponse(w, http.StatusCreated, "File Uploaded successfully!")

	return "/files/" + handle.Filename
}

func jsonResponse(w http.ResponseWriter, code int, message string) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	//fmt.Println(w, message)
}

func handlePostCreatorImage(w http.ResponseWriter, r *http.Request) string {
	//upload single image file
	file, handle, err := r.FormFile("creator_image")
	if err != nil {
		fmt.Println(err)
		fmt.Println("this is handle : ", handle)
		//panic(err)
	}

	defer file.Close()

	mimeType := handle.Header.Get("Content-Type")
	var creatorImgUrl string
	switch mimeType {
	case "image/jpeg":
		creatorImgUrl = saveFile(w, file, handle)
	case "image/png":
		creatorImgUrl = saveFile(w, file, handle)
	default:
		jsonResponse(w, http.StatusBadRequest, "The format file is not valid")
		return ""
	}
	return creatorImgUrl
}
