package models

import (
	"errors"
)

var (
	ErrorUserNotFound = errors.New("User Not Found")
	ErrorInvalidLogin = errors.New("Invalid Login")
)

type User struct {
	key string
}

/*
func NewUser(username string, hash []byte)(*User, error){
	id, err := client.Incr("user:next-id").Result()
	if err != nil{
		return nil, err
	}
	key := fmt.Sprintf("user:%d", id)
	pipe := client.Pipeline()
	pipe.HSet(key, "id", id)
	pipe.HSet(key, "username", username)
	pipe.HSet(key, "hash", hash)
	pipe.HSet("user:by-username", username, id)
	_, err = pipe.Exec()
	if err != nil{
		return nil, err
	}
	return &User{key}, nil
}

func (user *User) GetUsername() (string, error){
	return client.HGet(user.key, "username").Result()
}

func (user *User) GetHash() ([]byte, error){
	return client.HGet(user.key, "hash").Bytes()
}

func (user *User) Authenticate(password string) error{
	hash, err := user.GetHash()
	if err != nil{
		return err
	}
	err = bcrypt.CompareHashAndPassword(hash, []byte(password))
	if err == bcrypt.ErrMismatchedHashAndPassword{
		return ErrorInvalidLogin
	}
	return err
}

func GetUserByUsername(username string) (*User, error){
	id, err := client.HGet("user:by-username", username).Int64()
	if err == redis.Nil{
		return nil, ErrorUserNotFound
	} else if err != nil{
		return nil, err
	}
	key := fmt.Sprintf("user:%d", id)
	return &User{key}, nil
}

func RegisterUser(username, password string) error{
	cost := bcrypt.DefaultCost
	hash, err := bcrypt.GenerateFromPassword([]byte(password), cost)
	if err != nil{
		return err
	}
	_, err = NewUser(username, hash)
	return client.Set("user:" + username, hash, 0).Err()
}

func AuthenticateUser(username, password string) error{
	user, err := GetUserByUsername(username)
	if err != nil{
		return err
	}
	return user.Authenticate(password)
}
*/
