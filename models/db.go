package models

import (
	"database/sql"
	"fmt"
)

const (
	host     = "localhost"
	port     = 5432
	user     = "postgres"
	password = "postgres"
	dbname   = "postgres"
)

func GetPsql() (*sql.DB, error) {
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", host, port, user, password, dbname)

	psql, err := sql.Open("postgres", psqlInfo)
	if err != nil {
		return nil, err
	}
	//defer psql.Close()
	err = psql.Ping()
	if err != nil {
		return nil, err
	}
	return psql, nil
}
