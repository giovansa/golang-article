package models

import (
	"time"
)

type Article struct {
	Title           string    `json:"title"`
	Text            string    `json:"article_text"`
	CreatorName     string    `json:"creator_name"`
	CreatorImageUrl string    `json:"creator_img_url"`
	ImageUrl        string    `json:"article_img_url,omitempty"`
	Category        string    `json:"category"`
	CreatedAt       time.Time `json:"created_at"`
	UpdatedAt       time.Time `json:"updated_at"`
}

type Articles []Article

func NewArticle() *Article {
	return &Article{
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}
}
